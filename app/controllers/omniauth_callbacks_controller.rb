class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  include Devise::Controllers::Rememberable
  skip_before_filter :verify_authenticity_token

  def auth
    fields = request.env["omniauth.auth"]
    if user_signed_in?
      redirect_to user_path(self.current_user)
      return
    elsif (@user = User.authenticate(fields)) && (!@user.new_record?)
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => fields["provider"].titleize
      @user.remember_me = true
      @user.extend_remember_period = true
      remember_me(@user)
      sign_in_and_redirect(@user, :event => :authentication)
      return true
    end
    redirect_to new_session_path(:user)
  end
end