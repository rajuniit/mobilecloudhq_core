module Api
  module V1
    class ApiController < ApplicationController

      API_VERSION = '1.1'

      protected

      def handle_api_response(object, options={})
        content = {:json => prepare_api_response_data(object, options).to_json}
        response.headers['MCHQ-API-VERSION'] = API_VERSION

        respond_to do |f|
          f.any(:json, :js, :text, :html) { render content}
        end
      end

      def prepare_api_response_data(object, options)
        {}.tap do |api_response|
          if object.respond_to? :each
            api_response[:type] = 'List'
            api_response[:from] = params[:from].to_i if params[:from].present?
            api_response[:to] = params[:to].to_i if params[:to].present?
            api_response[:total] = 0
            api_response[:per_page] = params[:per_page].present? ? params[:per_page].to_i : App_Config.api_limit.to_i
            api_response[:objects] = object.map{|o| o.to_api_hash(options)}
          else
            api_response.merge!(object.to_api_hash(options))
          end

        end
      end

      def api_range
        from = params[:from]
        to = params[:to]

        if from and to
          {:reference_id.gte => from, :reference_id.lte => to}
        elsif from
          {:reference_id.gte => from}
        elsif to
          {:reference_id.lte => to}
        else
          {}
        end
      end

      def api_limit
        per_page = params[:per_page] && params[:per_page].to_i
        if per_page
          per_page == 0 ? nil : per_page
        else
          App_Config.api_limit
        end
      end

      def api_error(status_code, options={})
         errors = {}
         errors[:type] = options[:type] if options[:type]
        errors[:message] = options[:message] if options[:message]
        content = {:json => {:errors => errors}.to_json, :status => status_code}

         response.headers['MCHQ-API-VERSION'] = API_VERSION

         respond_to do |f|
           f.any(:json, :js, :text, :html) { render content}
         end
      end

    end
  end

end