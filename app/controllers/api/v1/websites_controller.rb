module Api
  module V1
    class WebsitesController < Api::V1::ApiController

      before_filter :load_website, :except => [:index]
      def index
        @websites = Website.where(api_range)
        .where(add_condition('domain'))
        .where(add_condition('subdomain'))
        .where(add_condition('status'))
        .limit(api_limit)
        handle_api_response @websites
      end

      def show
        handle_api_response @website
      end

      protected

      def load_website
        @website = Website.find_by_slug_or_id(params[:id])
        api_error :not_found, :type => 'ObjectNotFound', :message => 'Website not found' unless @website
      end

      def add_condition(key)
         if params[key].present?
           {:"#{key}" => params[key]}
         end
      end

    end
  end
end
