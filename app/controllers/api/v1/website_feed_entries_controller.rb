module Api
  module V1
    class WebsiteFeedEntriesController < Api::V1::ApiController

      before_filter :check_params, :only => [:index]
      before_filter :load_feed_entry, :except => [:index]
      def index
        @website_feed_entries = WebsiteFeedEntry.where(api_range)
                                                .where(:website_id => params[:website_id])
                                                .limit(api_limit)

        handle_api_response @website_feed_entries
      end

      def show
         handle_api_response @feed_entry
      end

      protected

      def load_feed_entry
        @feed_entry = WebsiteFeedEntry.find_by_slug_or_id(params[:id])
        api_error :not_found, :type => 'ObjectNotFound', :message => 'Feed not found' unless @feed_entry
      end
      def check_params
          api_error :not_found, :type => 'ObjectNotFound', :message => 'website_id param required' unless params[:website_id]
      end
    end
  end
end
