class WebsitesController < ApplicationController
  before_filter :authenticate_user!

  def index
    @websites = Website.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @websites }
    end
  end

  def show
    @website = Website.find_by_slug_or_id(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @website }
    end
  end

  def new
    @website = Website.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @website }
    end
  end

  def edit
    @website = Website.find_by_slug_or_id(params[:id])
  end

  def create
    @website = Website.new(params[:website])
    @website.user = current_user
    respond_to do |format|
      if @website.save
        WebsiteFeed.create_feed_and_entry(@website.website_feed_url, @website)
        format.html { redirect_to @website, notice: 'Website was successfully created.' }
        format.json { render json: @website, status: :created, location: @website }
      else
        if @website.errors.size > 0
          flash[:error] = Utility.prepare_validation_message(@website.errors)
        end

        format.html { render action: "new" }
        format.json { render json: @website.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @website = Website.find_by_slug_or_id(params[:id])
    @website.user = current_user
    respond_to do |format|
      if @website.update_attributes(params[:website])
        format.html { redirect_to @website, notice: 'Website was successfully updated.' }
        format.json { head :no_content }
      else
        if @website.errors.size > 0
          flash[:error] = Utility.prepare_validation_message(@website.errors)
        end
        format.html { render action: "edit" }
        format.json { render json: @website.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @website = Website.find_by_slug_or_id(params[:id])
    @website.destroy

    respond_to do |format|
      format.html { redirect_to websites_url, notice: 'Website Deleted Successfully' }
      format.json { head :no_content }
    end
  end
end
