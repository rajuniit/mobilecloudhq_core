class WebsiteRssCrawler

  @queue = :website_rss_crawler

  def self.perform(url, website_id)
    website = Website.find(website_id)
    feed = Feedzirra::Feed.fetch_and_parse(url)

    updated_feed = Feedzirra::Feed.update(feed)
    website_feed = WebsiteFeed.where(:website_id => website_id).first
    if updated_feed.updated?
      entries = updated_feed.new_entries
      entry_array = []
      entries.each do |entry|
        entry_hash = {:url => entry.url, :title => entry.title,
                      :author => entry.author, :summary => entry.summary,
                      :content => entry.content, :published => entry.published,
                      :categories => entry.categories,
                      :website => website}
        entry_array << entry_hash
      end

      website_feed.website_feed_entries_attributes = entry_array

      website_feed.save
    end
  end
end