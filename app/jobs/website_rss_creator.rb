class WebsiteRssCreator

  @queue = :website_rss_creator

  def self.perform(url, website_id)
    website = Website.find(website_id)
    feed = Feedzirra::Feed.fetch_and_parse(url)

    website_feed = WebsiteFeed.new
    website_feed.title = feed.title
    website_feed.url = feed.url
    website_feed.feed_url = feed.feed_url
    website_feed.etag = feed.etag
    website_feed.last_modified = feed.last_modified
    website_feed.website = website

    #feed entry
    entries = feed.entries
    entry_array = []
    entries.each do |entry|
      entry_hash = {:url => entry.url, :title => entry.title,
                    :author => entry.author, :summary => entry.summary,
                    :content => entry.content, :published => entry.published,
                    :categories => entry.categories,
                    :website => website}
      entry_array << entry_hash
    end

    website_feed.website_feed_entries_attributes = entry_array

    website_feed.save
  end
end