class Website
  include Mongoid::Document
  include Mongoid::Timestamps

  include MongoidExt::Slugizer
  include MongoidExt::Storage
  include MongoidExt::Filter

  field :name,              :type => String
  field :subdomain,         :type => String
  field :domain,            :type => String
  index :domain,            :unique => true
  field :status,            :type => String, :default => "active"
  field :language,          :type => String, :default => 'en'

  slug_key :name,           :unique => true
  filterable_keys           :name

  field :description,       :type => String
  field :deleted_at,        :type => Time

  field :current_theme, :type => String
  references_many :themes, :dependent => :destroy, :validate => false

  referenced_in   :user, :class_name => "User"

  field :website_feed_url, :type => String

  validates_presence_of :user, :name, :subdomain, :website_feed_url
  validates_uniqueness_of :name, :subdomain

  validates_length_of :name, :in => 3..40
  validates_length_of :description, :in => 3..1000, :allow_blank => true
  validates_format_of :subdomain, :with => /^[a-z0-9\-]+$/i
  validates_length_of :subdomain, :in => 3..32
  validates_length_of :domain, :in => 3..32, :if => :domain?

  validates_uniqueness_of :domain, :allow_blank => true,
                          :message => I18n.t("mongo_document.models.duplicate_domain_message"),
                          :if => :domain?

  validates_exclusion_of :subdomain,
                         :in => App_Config.blacklist_website_name,
                         :message =>   I18n.t("mongo_document.models.subdomain_black_list")


  validate :init_domain, :on => :create

  auto_increment :reference_id

  def to_api_hash(options = {})
    website_hash = {
        :id => _id.to_s,
        :name => name,
        :domain => domain,
        :subdomain => subdomain,
        :status => status,
        :website_feed_url => website_feed_url,
        :current_theme => current_theme,
        :created_at => created_at.to_s,
        :updated_at => updated_at.to_s
    }

    website_hash[:type] = self.class.to_s

    website_hash
  end

  protected

  def init_domain
    if domain.blank?
      self[:domain] = "#{self[:subdomain]}.#{App_Config.app_domain}"
    end
  end

end