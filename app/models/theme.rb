class Theme
  include Mongoid::Document
  include MongoidExt::Storage
  include Mongoid::Timestamps

  identity :type => String
  field :name, :type => String
  field :description, :type => String, :default => ""

  belongs_to :website

  validates_uniqueness_of :name, :allow_blank => false
  validates_presence_of :name

end
