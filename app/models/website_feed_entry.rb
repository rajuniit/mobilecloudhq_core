class WebsiteFeedEntry

  include Mongoid::Document
  include MongoidExt::Storage
  include Mongoid::Timestamps

  include MongoidExt::Slugizer

  field :url,         :type => String
  field :title,       :type => String
  field :author,      :type => String
  field :summary,     :type => String
  field :content,     :type => String
  field :published,   :type => Time
  field :categories,  :type => Array

  slug_key :title, :unique => true

  auto_increment :reference_id

  validates_presence_of :url, :title

  belongs_to :website_feed
  
  referenced_in :website

  def to_api_hash(options = {})
    feed_hash = {
        :id => _id,
        :url => url,
        :title => title,
        :author => author,
        :summary => summary,
        :content => content,
        :published => published,
        :categories => categories,
        :created_at => created_at.to_s,
        :updated_at => updated_at.to_s
    }

    feed_hash[:type] = self.class.to_s

    feed_hash
  end
end