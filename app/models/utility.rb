class Utility

  def self.prepare_validation_message(error)
    errors = ""
    error.full_messages.each do |msg|
      errors << "#{msg} <br />"
    end
    errors
  end
end