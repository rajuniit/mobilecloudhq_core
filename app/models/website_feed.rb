class WebsiteFeed

  include Mongoid::Document
  include MongoidExt::Storage
  include Mongoid::Timestamps

  include MongoidExt::Slugizer

  field :url, :type => String
  field :feed_url, :type => String
  field :title, :type => String
  field :etag, :type => String
  field :last_modified, :type => DateTime

  slug_key :title, :unique => true


  validates_presence_of :url, :feed_url

  referenced_in :website

  references_many :website_feed_entries, :dependent => :destroy, :validate => false, :autosave => true
  accepts_nested_attributes_for :website_feed_entries

  def self.create_feed_and_entry(url, website)

    Resque.enqueue(WebsiteRssCreator, url, website._id)
    Resque.set_schedule('website-rss-crawler-' + website._id.to_s, {
        :class => 'WebsiteRssCrawler',
        :every => '30mins',
        :args => [url, website._id]
    })
  end
end