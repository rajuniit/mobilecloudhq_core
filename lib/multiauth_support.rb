module MultiauthSupport
  extend ActiveSupport::Concern

  included do

    field :facebook_id, :type => String
    field :facebook_token, :type => String
    field :facebook_profile, :type => String

    field :auth_keys, :type => Array, :default => []
    field :user_info, :type => Hash, :default => {}
  end

  module ClassMethods
    def authenticate(fields)
      provider = fields["provider"]
      uid = fields["uid"] || fields["extra"]["raw_info"]["id"]
      auth_key = "#{provider}_#{uid}"
      user = User.where({:auth_keys.in => [auth_key]}).first
      if user.nil?
        user = User.new
        user.auth_keys = [auth_key]
        user.user_info[provider] = fields["info"]

        if user.email.blank?
          user.email = user.user_info[provider]["email"]
        end

        user.send("handle_#{provider}", fields) if user.respond_to?("handle_#{provider}", true)

        if !user.save
          Rails.logger.info "Invalid new user from #{provider}: #{user.errors.full_messages.inspect}"
        end
      end
      user.check_user_info(fields, provider)
      user
    end
  end

  def check_user_info(fields, provider)
    user = self

    if provider == 'facebook' && ((user.user_info["facebook"] && user.user_info["facebook"]["old"]) ||
              (user.user_info["facebook"].blank?))
      user.user_info["facebook"] = fields["info"]
      user.save(:validate => false)
    end
  end

  private

  def handle_facebook(fields)
    user_info = fields["extra"]["raw_info"].clone
    self.facebook_id = fields["uid"].to_s
    self.facebook_token = fields["credentials"]["token"].to_s
    self.facebook_profile = fields["info"]["urls"]["Facebook"].to_s

    if self.email.blank?
      self.email = user_info["email"]
    end
  end
end