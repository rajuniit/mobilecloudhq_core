MobilecloudhqCore::Application.routes.draw do

  resources :websites

  root :to => "home#index"

  devise_for :users

  resources :users, :only => :show

  namespace :api do
    namespace :v1 do
      resources :users
      resources :website_feed_entries
      resources :websites
    end
  end

  mount Resque::Server, :at => "/resque"
  devise_scope :user do
    match '/users/auth/:provider/callback', :to => 'omniauth_callbacks#auth', :as => :omniauth_callback, :via => [:get, :post, :put]
    match '/users/auth/:provider', :to => 'omniauth_callbacks#auth', :as => :omniauth_auth, :via => [:get]
  end


end
