Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, App_Config.facebook['key'], App_Config.facebook['secret'] if App_Config.facebook['activate']
end