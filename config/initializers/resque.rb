require 'resque_scheduler'

resque_config = YAML.load_file("#{Rails.root}/config/resque.yml")
resque_config = resque_config[ENV['RAILS_ENV'] || 'development']

Resque.redis = resque_config['redis_server']
Resque.redis.namespace = resque_config['redis_namespace']

# If you want to be able to dynamically change the schedule,
# uncomment this line.  A dynamic schedule can be updated via the
# Resque::Scheduler.set_schedule (and remove_schedule) methods.
# When dynamic is set to true, the scheduler process looks for
# schedule changes and applies them on the fly.
# Note: This feature is only available in >=2.0.0.
Resque::Scheduler.dynamic = true
Dir["#{Rails.root}/app/jobs/*.rb"].each { |file| require file}

#Resque.schedule = YAML.load_file(Rails.root.join('config', 'resque_schedule.yml'))


