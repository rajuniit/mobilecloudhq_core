Feature: Delete a Website

  Background:
    Given "rajuniit@gmail.com" exists and is logged in
    And I go to the new website page
    And the following websites exists:
      | name   | subdomain     | website_feed_url |
      | raju   | rajumazumder  | http://www.rajumazumder.com/feed/rss/ |
      | raju 1 | rajumazumder1 | http://www.rajumazumder.com/feed/rss/ |
      | raju 2 | rajumazumder2 | http://www.rajumazumder.com/feed/rss/ |
    And I press "Create Website"
    And I go to website listing page
    And I should see "Websites"

  Scenario: Raju deletes a website
    When I follow "Destroy"
    Then I should see "Website Deleted Successfully"
