Feature: Logging In

  Background:
    Given I am currently "rajuniit@gmail.com"
    And I go to the login page
    And I fill in "Email" with "rajuniit@gmail.com"
    And I fill in "Password" with "hacker"
    And I press "Sign in"

  Scenario: Raju can see his website listing
    When I go to the websites page
    Then I should see "Websites"

  Scenario: rajuniit@gmail.com logout and try to login with wrong password and username
    When I log out
    Then I should see "Home"
    And I go to the login page
    And I fill in "Email" with "hacker"
    And I fill in "Password" with "raju"
    And I press "Sign in"
    Then I should see "Invalid email or password."