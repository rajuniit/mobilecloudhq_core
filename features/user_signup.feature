Feature: Signing Up
  In Order to access my dashboard
  As an user I want to signup
  I want to create and manage website

  Scenario: Raju successfully signs up
    When I go to the signup page
    And I fill in the following:
      | Email | rajuniit@gmail.com |
      | Password | hacker          |
      | Password confirmation | hacker  |
    And I press "Create Website"
    Then I should see "Welcome! You have signed up successfully."