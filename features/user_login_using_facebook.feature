Feature: User login facebook
  Scenario: Raju can login using his facebook account
    Given I authenticate on "facebook" with "rajuniit@gmail.com" account
    When I go to the login page
    Then I should see "Sign in with facebook"
    And I go to  "Sign in with facebook"
    Then I should see "Successfully authenticated from Facebook account."