Feature: View a website
  In order to see the website details
  As an api client
  I want to see the a website detail information

  Scenario: show a website
    Given the following websites exists:
      | name   | subdomain     | website_feed_url                      | _id    |
      | raju   | rajumazumder  | http://www.rajumazumder.com/feed/rss/ | 501e90da5f097d1a00000005  |
      | raju 1 | rajumazumder1 | http://www.rajumazumder.com/feed/rss/ | 502167c45f097d164b000009  |
      | raju 2 | rajumazumder2 | http://www.rajumazumder.com/feed/rss/ | 50169e185f097d07c5000006  |
    When I GET "/api/v1/websites/501e90da5f097d1a00000005"
    Then I should get a successful response
    And the response should include id "501e90da5f097d1a00000005"

  Scenario: show a 404 response if website not found
    When I GET "/api/v1/websites/501e90da5f097d1a00000005"
    Then I should get a 404 response
    And the error message is "Website not found"
