Feature: Website feed entry Listing
  In order to see the available feed entries of my website
  As an api client
  I want to get all the available feed entries


  Scenario: Get a list of all feed entry of a website
    Given two website feed exists of a website nameed "rajumazumder"
    When I GET "/api/v1/website_feed_entries" with the param website_id
    Then I should get a successful response
    And the response should include 2 website feed
    And the response should include the object name "WebsiteFeedEntry"

  Scenario: Get a list of websites with per_page limits
    Given two website feed exists of a website nameed "rajumazumder"
    When I GET "/api/v1/website_feed_entries" with the param website_id and per_page
    Then I should get a successful response
    And the response should include 2 website feed
    And the response should include the object name "WebsiteFeedEntry"

  Scenario: Get a list of websites using limits and offset
    Given two website feed exists of a website nameed "rajumazumder"
    When I GET "/api/v1/website_feed_entries" with the params website_id, per_page, from
    Then I should get a successful response
    And the response should include 1 websites

