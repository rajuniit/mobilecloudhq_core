Feature: Website Listing
  In order to see the available websites
  As an api client
  I want to get all the available websites


  Scenario: Get a list of all websites
    Given the following websites exists:
      | name   | subdomain     | website_feed_url |
      | raju   | rajumazumder  | http://www.rajumazumder.com/feed/rss/ |
      | raju 1 | rajumazumder1 | http://www.rajumazumder.com/feed/rss/ |
      | raju 2 | rajumazumder2 | http://www.rajumazumder.com/feed/rss/ |
    When I GET "/api/v1/websites"
    Then I should get a successful response
    And the response should include 3 websites
    And the response should include the object name "Website"

  Scenario: Get a list of websites with per_page limits
    Given the following websites exists:
      | name   | subdomain     | website_feed_url |
      | raju   | rajumazumder  | http://www.rajumazumder.com/feed/rss/ |
      | raju 1 | rajumazumder1 | http://www.rajumazumder.com/feed/rss/ |
      | raju 2 | rajumazumder2 | http://www.rajumazumder.com/feed/rss/ |
    When I GET "/api/v1/websites" with the following:
    """
    {"per_page" => "2"}
    """
    Then I should get a successful response
    And the response should include 2 websites
    And the response should include the object name "Website"

  Scenario: Get a list of websites using limits and offset
    Given the following websites exists:
      | name   | subdomain     | website_feed_url |
      | raju   | rajumazumder  | http://www.rajumazumder.com/feed/rss/ |
      | raju 1 | rajumazumder1 | http://www.rajumazumder.com/feed/rss/ |
      | raju 2 | rajumazumder2 | http://www.rajumazumder.com/feed/rss/ |
    When I GET "/api/v1/websites" with the following:
    """
    {"per_page" => "1",
     "from" => "2"}
    """
    Then I should get a successful response
    And the response should include 1 websites

  Scenario: find a website using subdomain
    Given the following websites exists:
      | name   | subdomain     | website_feed_url |
      | raju   | rajumazumder  | http://www.rajumazumder.com/feed/rss/ |
      | raju 1 | rajumazumder1 | http://www.rajumazumder.com/feed/rss/ |
      | raju 2 | rajumazumder2 | http://www.rajumazumder.com/feed/rss/ |
    When I GET "/api/v1/websites" with the following:
    """
    {"subdomain" => "rajumazumder"}
    """
    Then I should get a successful response
    And the response should include 1 websites
    And the response should include subdomain "rajumazumder"

  Scenario: find a website using domain
    Given the following websites exists:
      | name   | subdomain     | website_feed_url                      | domain |
      | raju   | rajumazumder  | http://www.rajumazumder.com/feed/rss/ | rajumazumder.localhost.lan   |
      | raju 1 | rajumazumder1 | http://www.rajumazumder.com/feed/rss/ | rajumazumder1.localhost.lan  |
      | raju 2 | rajumazumder2 | http://www.rajumazumder.com/feed/rss/ | rajumazumder2.localhost.lan  |
    When I GET "/api/v1/websites" with the following:
    """
    {"domain" => "rajumazumder.localhost.lan"}
    """
    Then I should get a successful response
    And the response should include 1 websites
    And the response should include domain "rajumazumder.localhost.lan"

  Scenario: get a response without any data when subdomain not match
    When I GET "/api/v1/websites" with the following:
    """
    {"subdomain" => "rajumazumder"}
    """
    Then I should get a successful response
    And the response should include 0 websites



