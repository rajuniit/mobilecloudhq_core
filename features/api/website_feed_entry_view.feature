Feature: View a website feed entry
  In order to see the website feed entry details
  As an api client
  I want to see the a website feed entry information

  Scenario: show a website feed entry
    Given two website feed exists of a website nameed "rajumazumder"
    When I GET "/api/v1/website_feed_entries/" with the param website_id to get a single feed
    Then I should get a successful response
    And the response should include the requested website feed

  Scenario: show a 404 response if website feed entry not found
    When I GET "/api/v1/website_feed_entries/0987654321" with non-existing id to get a single feed
    Then I should get a 404 response
    And the error message is "Feed not found"
