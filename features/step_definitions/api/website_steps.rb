When /^I GET "([^"]*)"$/ do |path|
  get path
end

Then /^I should get a successful response$/ do
   last_response.status.should == 200
end

Then /^the response should include (\d+) websites$/ do |number|
  list = JSON.parse(last_response.body)
  list['objects'].length.should == number.to_i
end

Then /^the response should include the object name "([^"]*)"$/ do |object|
  list = JSON.parse(last_response.body)
  list['objects'].each { |o| o['type'].should == object}
end

When /^I GET "([^"]*)" with the following:$/ do |path, body|
  get path, eval(body)
end

Then /^the response should include subdomain "([^"]*)"$/ do |subdomain|
  JSON.parse(last_response.body)['objects'].map { |a| a['subdomain'].to_s}.should == [subdomain]
end

Then /^the response should include domain "([^"]*)"$/ do |domain|
  JSON.parse(last_response.body)['objects'].map { |a| a['domain'].to_s}.should == [domain]
end

Then /^the response should include id "([^"]*)"$/ do |id|
  JSON.parse(last_response.body)['id'].should == id
end

Then /^I should get a (\d+) response$/ do |status_code|
  last_response.status.should == status_code.to_i
end

Then /^the error message is "([^"]*)"$/ do |message|
  JSON.parse(last_response.body)['errors']['message'].should == message
end
