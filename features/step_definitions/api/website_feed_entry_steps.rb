Given /two website feed exists of a website nameed "([^"]*)"$/ do |website_name|
  @website = FactoryGirl.create(:website, :name => website_name)
  @website_feed_entry = FactoryGirl.create(:website_feed_entry, :website => @website)
  @other_website_feed_entry = FactoryGirl.create(:website_feed_entry, :website => @website)
end

When /^I GET "([^"]*)" with the param website_id$/ do |path|
  get path, :website_id => @website._id.to_s
end

Then /^the response should include (\d+) website feed$/ do |number|
  list = JSON.parse(last_response.body)
  list['objects'].length.should == number.to_i
end

When /^I GET "([^"]*)" with the param website_id and per_page$/ do |path|
  get path, :website_id => @website._id.to_s, :per_page => 2
end

When /^I GET "([^"]*)" with the params website_id, per_page, from$/ do |path|
  get path, :website_id => @website._id.to_s, :per_page => 1, :from => 2
end

When /^I GET "([^"]*)" with the param website_id to get a single feed$/ do |path|
  get path + @website_feed_entry._id.to_s, :website_id => @website._id.to_s
end

Then /^the response should include the requested website feed$/ do
  JSON.parse(last_response.body)['id'].should == @website_feed_entry._id.to_s
end

When /^I GET "([^"]*)" with non\-existing id to get a single feed$/ do |path|
  @website = FactoryGirl.create(:website, :name => 'rajumazumder')
  get path, :website_id => @website._id.to_s
end