Given /^I am currently "([^"]*)"$/ do |email|
  @current_user = FactoryGirl.create(:user, :email => email)
end

When /^I log out$/ do
  visit('/users/sign_out')
end

Given /^I authenticate on "([^"]*)" with "([^"]*)" account$/ do |provider, account|
  data = load_data_from_yml(provider)
  OmniAuth.config.test_mode = true
  OmniAuth.config.mock_auth[provider.to_sym] = data
end


def load_data_from_yml(provider)
  config_file = Rails.root + "features/support/omni_auth.yml"
  data = YAML.load_file(config_file)
  data[provider]
end

