Given /^"([^"]*)" exists and is logged in$/ do |email|
   @current_user = FactoryGirl.create(:user, :email => email)
   visit("/users/sign_in")
   fill_in "Email", :with => email
   fill_in "Password", :with => 'hacker'
   click_button "Sign in"
end

Given /^"([^"]*)" subdomain already taken$/ do |subdomain|
  @website = FactoryGirl.create(:website, :subdomain => subdomain)
end