Feature: Creating a website

  Background:
    Given "rajuniit@gmail.com" exists and is logged in
    And I go to the new website page

  Scenario Outline: Raju creates two valid websites and fails to create an invalid website
    When I fill in the following:
      | Name | <name> |
      | Subdomain  | <subdomain>  |
      | Website Feed Url | <website_feed_url> |
    And I press "Create Website"
    Then I should see "<flash>"

    Examples:
      | name          | subdomain         | website_feed_url                           | flash                            |
      | rajumazumder  | rajumazumer | http://www.rajumazumder.com/blog/feed/rss/ | Website was successfully created.|
      | emran         | emran       | http://www.rajumazumder.com/blog/feed/rss/ | Website was successfully created.|
      | ra            | rajumazumer | http://www.rajumazumder.com/blog/feed/rss/ | Name is too short (minimum is 3 characters)|

  Scenario: I don't fill in subdomain name
    When I fill in "Name" with "Raju Mazumder"
    And  I fill in "Website Feed Url" with "http://www.rajumazumder.com/blog/feed/rss/"
    And  I press "Create Website"
    Then I should see "Subdomain can't be blank"

  Scenario: Subdomain already taken
    Given "rajumazumder" subdomain already taken
    When I fill in "Name" with "Raju Mazumder"
    And I fill in "Subdomain" with "rajumazumder"
    And I fill in "Website Feed Url" with "http://www.rajumazuder.com/blog/feed/rss"
    And I press "Create Website"
    Then I should see "Subdomain is already taken"