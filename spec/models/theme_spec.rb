require "spec_helper"

describe Theme do

  describe "Validations" do
    it { should validate_presence_of(:name) }

    it { should validate_uniqueness_of(:name) }
  end

  describe "Associations" do
    it { should belong_to(:website)}
  end

end