require 'spec_helper'

describe User do

  describe "Validations" do
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
    it { should validate_length_of(:email).within(6..100) }

    it { should validate_presence_of(:password) }
    it { should validate_confirmation_of(:password) }
  end

  describe "Authentication" do
    before do
      @user = FactoryGirl.create(:user)
    end
  end

  describe "It should authenticate via omniauth" do
    before do
      @data = auth_data
    end
    it "shuould success for valid data" do
      user = User.authenticate(@data)
      user.email == @data['email']
    end
  end

end