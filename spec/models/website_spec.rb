require "spec_helper"

describe Website do

  describe "Validations" do
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:subdomain) }
    it { should validate_presence_of(:website_feed_url) }

    it { should validate_uniqueness_of(:name)}
    it { should validate_uniqueness_of(:subdomain)}
    it { should validate_uniqueness_of(:domain)}

    it { should validate_length_of(:name).within(3..40)}
    it { should validate_length_of(:description).within(3..1000)}
    it { should validate_length_of(:subdomain).within(3..32)}
    it { should validate_length_of(:domain).within(3..32)}

    it {should validate_exclusion_of(:subdomain).to_not_allow(App_Config.blacklist_website_name)}
  end

  describe "Associations" do
    it { should have_many(:themes)}
    it { should belong_to(:user)}
  end

  describe "Website Creation" do
    before do
      @website = FactoryGirl.create(:website)
    end
    it "should assign app config domain if domain is blank" do
       @website.domain == "#{@website.subdomain}.#{App_Config.app_domain}"
    end

    it "should have a method to convert document object as a hash" do
      hash = @website.to_api_hash
      hash['name'] == @website.name
    end
  end

end