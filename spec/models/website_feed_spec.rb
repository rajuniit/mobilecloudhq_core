require "spec_helper"

describe WebsiteFeed do

  describe "Validations" do
    it { should validate_presence_of(:url) }
    it { should validate_presence_of(:feed_url) }
  end

  describe "Associations" do
    it { should have_many(:website_feed_entries)}
    it { should belong_to(:website)}
  end

  describe "Add Resque job for to create and parse rss feed and set a schedule to update that rss feed" do

    before do
      ResqueSpec.reset!
      @website = FactoryGirl.create(:website)
    end

    it "should add WebsiteRssCreatoer.perform to do the :website_rss_creator queue" do
      WebsiteFeed.create_feed_and_entry(@website.website_feed_url, @website)
      WebsiteRssCreator.should have_queued(@website.website_feed_url, @website._id)
    end
  end


end