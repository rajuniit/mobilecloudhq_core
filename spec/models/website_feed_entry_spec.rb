require "spec_helper"

describe WebsiteFeedEntry do

  describe "Validations" do
    it { should validate_presence_of(:url) }
    it { should validate_presence_of(:title) }
  end

  describe "Associations" do
    it { should belong_to(:website)}
  end

  describe "Website Feed Entry Creation" do
    before  do
       @website_feed_entry = FactoryGirl.create(:website_feed_entry)
    end

    it "should have a method to convert document object as a hash" do
      hash = @website_feed_entry.to_api_hash
      hash['title'] == @website_feed_entry.title
    end
  end

end