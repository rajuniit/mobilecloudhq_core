#require 'factory_girl'

FactoryGirl.define do

  sequence :email do |n|
    "email#{n}@example.com"
  end

  sequence :name do |n|
    "website#{n}"
  end

  sequence :title do |n|
    "post-title-#{n}"
  end

  sequence :subdomain do |n|
    "rajumazumder-#{n}"
  end

  factory :user do
    email
    password 'hacker'
    password_confirmation 'hacker'
  end

  factory :website do
    name
    subdomain
    website_feed_url 'http://www.rajumazumder.com/blog/rss'
    association :user, factory: :user
  end

  factory :website_feed do
    title 'personal site'
    url 'http://www.rajumazumder.com'
    feed_url 'http://www.rajumazumder.com/blog/rss'
    association :website, factory: :website
  end

  factory :website_feed_entry do
     title
     url 'http://www.rajumazumder.com'
     association :website, factory: :website
     association :website_feed, factory: :website_feed
  end

end