module OmniAuthHelper

  protected

  def auth_data
    data = load_data_from_yml
  end

  def auth_data_by_provider(provider)
    data = load_data_from_yml
    return data[provider]
  end

  def load_data_from_yml
    config_file = Rails.root + "spec/support/omni_auth.yml"
    data = YAML.load_file(config_file)
  end
end

