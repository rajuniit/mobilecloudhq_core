module AuthenticatedTestHelper
  protected

  def stub_authentication(user, *args)
    @user = FactoryGirl.create(user, *args)  unless user.nil? or user.is_a? User
    Thread.current[:current_user] = @user
    sign_in @user
    controller.stub!(:current_user).and_return(@user)
    @user
  end
end