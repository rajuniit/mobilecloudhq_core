require 'spec_helper'

describe WebsitesController do
  include Devise::TestHelpers

  before(:each) do
    @user = FactoryGirl.create(:user)
    @website = FactoryGirl.create(:website)
    stub_authentication(@user)
  end

  describe "Get index" do
    it "should be successful" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "should be successful" do
      get 'show', :id => @website.id
      response.should be_success
      assigns[:website].id.should == @website.id
    end
  end

  describe "GET 'new'" do
    it "should be successful" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do

    it "should create website successfully " do
      attrs = FactoryGirl.attributes_for(:website)
      post :create, :website => attrs
      response.should redirect_to website_path(:id => assigns[:website].slug)
    end
  end

  describe "GET 'edit'" do
     it "should be successful" do
       get 'edit', :id => @website.id
       response.should be_success
     end
  end

  describe "PUT update" do
    before(:each) do
      @website_attrs = FactoryGirl.attributes_for(:website)
    end

    it "should be successfull" do
      @website_attrs.delete("title")
      put 'update', :id => @website.id, :website => @website_attrs
      response.should redirect_to website_path(:id => assigns[:website].slug)
    end
  end

  describe "DELETE 'destroy'" do
    it "should be successful" do
      delete 'destroy', :id => @website.id
      response.should redirect_to websites_url
    end
  end


end
