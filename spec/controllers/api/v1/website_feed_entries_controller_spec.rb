require 'spec_helper'

describe Api::V1::WebsiteFeedEntriesController do
  before do
    @website = FactoryGirl.create(:website, :subdomain => 'rajumazumder')
    @website_feed_entry = FactoryGirl.create(:website_feed_entry, :website => @website)
    @other_website_feed_entry = FactoryGirl.create(:website_feed_entry, :website => @website)
  end

  describe "#index" do
    it "shows all the feed those are associated with @website" do
      get :index, :website_id => @website['_id'].to_s
      response.should be_success
      list = JSON.parse(response.body)
      list['type'].should == 'List'
      list['objects'].each { |o| o['type'].should == 'WebsiteFeedEntry'}
      list['objects'].length.should == 2
    end

    it "limits feed entries using per_page params" do
      get :index, :website_id => @website['_id'].to_s, :per_page => 1
      response.should be_success
      JSON.parse(response.body)['objects'].length.should == 1
    end

    it 'limits and offsets feed entry' do
      get :index, :website_id => @website['_id'], :from => 2, :per_page => 1
      response.should be_success
      JSON.parse(response.body)['objects'].map { |a| a['id'].to_s}.should == [@other_website_feed_entry['_id'].to_s]
    end

    it "should return blank list if no feed entries found" do
      get :index, :website_id => 'mor shala'
      response.should be_success
      data = JSON.parse(response.body)
      data['total'].should == 0
    end
  end

  describe "#show" do
    it "should shows a website feed entry" do
      get :show, :id => @website_feed_entry['_id'].to_s
      response.should be_success

      data = JSON.parse(response.body)
      data['type'].should == 'WebsiteFeedEntry'
      data['id'].to_s.should == @website_feed_entry['_id'].to_s
    end

    it "should not show a website feed entry which does not exist" do
      get :show, :id => 'mor shala'
      response.status.should == 404
      JSON.parse(response.body)['errors']['type'].should == 'ObjectNotFound'
    end
  end
end
