require 'spec_helper'

describe Api::V1::WebsitesController do
  before do
    @website = FactoryGirl.create(:website, :subdomain => 'rajumazumder')
    @other_website = FactoryGirl.create(:website, :subdomain => 'emran')
  end

  describe "#index" do
      it "shows all the websites those are created" do
        get :index
        response.should be_success
        list = JSON.parse(response.body)
        list['type'].should == 'List'
        list['objects'].each { |o| o['type'].should == 'Website'}
        list['objects'].length.should == 2
      end

      it "limits websites using per_page params" do
          get :index, :per_page => 1
          response.should be_success
          JSON.parse(response.body)['objects'].length.should == 1
      end

      it 'limits and offsets website' do
        get :index, :from => 2, :per_page => 1
        response.should be_success
        JSON.parse(response.body)['objects'].map { |a| a['id'].to_s}.should == [@other_website['_id'].to_s]
      end

      it "should return the website if subdomain match" do
        get :index, :subdomain => 'emran'
        response.should be_success
        JSON.parse(response.body)['objects'].map { |a| a['id'].to_s}.should == [@other_website['_id'].to_s]
      end

      it "should return the website if domain match" do
        get :index, :domain => 'emran.localhost.lan'
        response.should be_success
        JSON.parse(response.body)['objects'].map { |o| o['id'].to_s}.should == [@other_website['_id'].to_s]
      end

      it "should return blank list if subdomain not match" do
        get :index, :subdomain => 'ajaxray'
        response.should be_success
        data = JSON.parse(response.body)
        data['total'].should == 0
      end
  end

  describe "#show" do
    it "shold shows a website" do
      get :show, :id => @website['_id'].to_s
      response.should be_success

      data = JSON.parse(response.body)
      data['type'].should == 'Website'
      data['id'].to_s.should == @website['_id'].to_s
    end

    it "should not show a website which does not exist" do
      get :show, :id => 'mor shala'
      response.status.should == 404
      JSON.parse(response.body)['errors']['type'].should == 'ObjectNotFound'
    end
  end
end
