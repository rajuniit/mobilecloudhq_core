require 'spec_helper'

describe OmniauthCallbacksController do

  describe "GET auth" do
    before do
      OmniAuth.config.test_mode = true
      OmniAuth.config.mock_auth[:facebook] = auth_data
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
    end
    it "shuold login successfully for valid data" do
      get :auth, :provider => 'facebook'
      flash[:notice].should eql('Successfully authenticated from Facebook account.')
    end
  end
end